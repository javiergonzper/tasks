import json
import mock
import pytest
import sys
from main import app


_MOCK_TASKS = [{
    'id': 1,
    'title': 'Task #1'
}, {
    'id': 2,
    'title': 'Task #2'
}, {
    'id': 3,
    'title': 'Task #3'
}]

_MOCK_TASK = _MOCK_TASKS[0]

_BUILTIN_JSON_LOADS = json.loads


def _STUB_JSON_LOADS(data, **kwargs):

    """ Handle 'json.loads' encoding in a proper way (for Python prior to 3.6) """

    return _BUILTIN_JSON_LOADS(data.decode('utf-8'), **kwargs) \
        if sys.version_info.major == 3 and sys.version_info.minor < 6 and isinstance(data, bytes) \
        else _BUILTIN_JSON_LOADS(data, **kwargs)


@pytest.fixture
def client():

    """ Get the test client for the Flask application """

    app.testing = True
    return app.test_client()


@mock.patch('src.business.tasks.list', mock.Mock(return_value=_MOCK_TASKS))
def test_list_success(client):

    """ Should retrieve the tasks collection """

    response = client.get('/api/tasks', follow_redirects=True)
    assert response.status_code == 200
    tasks = response.json
    assert isinstance(tasks, list)
    assert len(tasks) == len(_MOCK_TASKS)


@mock.patch('json.loads', _STUB_JSON_LOADS)
@mock.patch('src.business.tasks.create', mock.Mock(return_value=_MOCK_TASK))
def test_create_success(client):

    """ Should create a task """

    data = {'title': _MOCK_TASK['title']}
    response = client.post('/api/tasks', json=data, follow_redirects=True)
    assert response.status_code == 201
    task = response.json
    assert isinstance(task, dict)
    assert 'id' in task
    assert task['title'] == data['title']


@mock.patch('src.business.tasks.delete', mock.Mock(return_value=None))
def test_delete_success(client):

    """ Should delete a task """

    task_id = _MOCK_TASK['id']
    response = client.delete('/api/tasks/{}'.format(task_id), follow_redirects=True)
    assert response.status_code == 204
    assert response.data.decode('utf-8') == ''
