-r requirements.txt
coverage==4.5.3
flake8==3.7.7
mock==2.0.0
pytest==4.4.0
